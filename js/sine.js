(function() {

    var points = {},
        points2 = {},
        counter = 0,
        scopeWidth = 750;
    var _sineWaveGenerator = function(canvas) {
        var getPosition = function(freq, amp, x) {
            return amp * Math.sin(freq * 0.1 * x) + 50; //offset;
        };
        var ctx = canvas.getContext("2d");
        ctx.lineWidth = 3;

        var x = 0,
            x2 = 0,
            y = getPosition(0),
            y2 = getPosition(0);

        var timeout = setInterval(function() {
            if(counter < scopeWidth) {
                if(monochord.showRoot) {
                    ctx.beginPath();
                    ctx.strokeStyle = "#ededed";
                    ctx.moveTo(x2, y2);
                    x2 += 1;
                    y2 = getPosition(1, 20, x2);
                    points2[x2] = y2;
                    ctx.lineTo(x2, y2);
                    ctx.stroke();
                }
                ctx.beginPath();
                ctx.strokeStyle = "#d6edbd";
                ctx.moveTo(x, y);
                x += 1;
                y = getPosition(monochord.percent, 40, x);
                points[x] = y;
                ctx.lineTo(x, y);
                ctx.stroke();
            } else {
                ctx.clearRect(0, 0, scopeWidth, scopeWidth);
                if(monochord.showRoot) {
                    ctx.beginPath();
                    ctx.strokeStyle = "#ededed";
                    points2[x2] = y2;
                    x2 += 1;
                    y2 = getPosition(1, 20, x2);
                    for(var j = 0; j < scopeWidth; j++) {
                        ctx.lineTo(j, points2[j + counter - scopeWidth]);
                    }
                    ctx.stroke();
                }

                ctx.beginPath();
                ctx.strokeStyle = "#d6edbd";
                points[x] = y;
                x += 1;
                y = getPosition(monochord.percent, 40, x);

                for(var i = 0; i < scopeWidth; i++) {
                    ctx.lineTo(i, points[i + counter - scopeWidth]);
                }
                ctx.stroke();
            }
            counter++;
        }, 1);
    };
     //public
    P2N.utils.namespace('monochord.sine', {
        start: function(canvas){
            _sineWaveGenerator(canvas);
        }
    });
}());