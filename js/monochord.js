(function(jQuery) {
    //private
    var note = Temper(),
        $notename, $hertz, $ratio, $cents, $intervalList, $rootFrequency, slider;

    var _positionNotes = function() {
        var tuning = note.temperament();
        var $intervals = jQuery('.intervals li');
        jQuery.each(tuning, function(idx, interval) {
            var ratio = interval;
            jQuery($intervals[idx]).animate({
                left: (ratio * 734) - 742
            });
            jQuery.data($intervals[idx], 'ratio', ratio);

        });
    };

    var _setupBindings = function() {
        var bindInput = function($el, percentClosure) {
            var processInput = function() {

                var percent = percentClosure();
                if(isNaN(percent) || !_update(percent, true)) {
                    $el.tooltip('show');
                }
                $intervalList.removeClass('selected');
            };
            $el.focus(function() {
                $el.tooltip('hide');
            }).keypress(function(e) {
                if(e.which == 13) {
                    processInput();
                }
            });
        };

        bindInput($hertz, function() {
            return $hertz.val() / monochord.rootFrequency;
        });
        bindInput($cents, function() {
            var centRatio = Math.pow(2, $cents.val() / 1200) * monochord.rootFrequency / monochord.rootFrequency;
            return centRatio;
        });
        bindInput($ratio, function() {
            var ratioArray = $ratio.val().split(":");
            return ratioArray[0] / ratioArray[1];
        });

        $hertz.tooltip({
            trigger: 'manual',
            title: 'Hertz need to be between ' + monochord.rootFrequency + ' and ' + monochord.rootFrequency * 2
        });
        $cents.tooltip({
            trigger: 'manual',
            title: 'Cents need to be between 0 and 1200'
        });
        $ratio.tooltip({
            trigger: 'manual',
            title: 'The ratio need to be between 1:1 and 2:1'
        });

    };
    
    var _update = function(percent, updateSlider) {
        if(percent < 1 || percent > 2) {
            return false;
        }
        note.freq(percent * monochord.rootFrequency);
        $notename.html(note.noteName());
        var hertz = note.freq();
        _playNotes(hertz);
        $hertz.val(hertz);
        $hertz.tooltip('hide');

        var ratio = P2N.utils.getRatioFromNumber(percent);
        $ratio.val(ratio);
        $ratio.tooltip('hide');

        var cents = note.offset(monochord.rootFrequency);
        $cents.val(cents);
        $cents.tooltip('hide');

        if(updateSlider) {
            slider.setValue(percent - 1);
        }

        monochord.percent = percent;

        return true;
    };

    var _updateTemperament = function(name) {
        note.temperament(name);
        _positionNotes();
        jQuery('.intervals li.selected').trigger('click');
    };

    var _playNotes = function(hertz) {
        monochord.pluck.playNote(hertz, 0.7);
        monochord.pluck.playNote(monochord.rootFrequency, 0.2);
    };

    //public
    P2N.utils.namespace('monochord', {
        showRoot: true,
        percent: 0,
        rootFrequency: 440,
        init: function() {
            $notename = jQuery('.note-name');
            $hertz = jQuery('.hertz');
            $ratio = jQuery('.ratio');
            $cents = jQuery('.cents');
            $intervalList = jQuery('.intervals li');
            $rootFrequency = jQuery('.root');
            $rootFrequency.val(monochord.rootFrequency);

            monochord.sine.start(jQuery("#sine")[0]);
            _setupBindings();
            _positionNotes(note.temperament());

            slider = new Dragdealer('slider', {
                animationCallback: function(x, y) {
                    $intervalList.each(function() {
                        if(jQuery(this).data('ratio').toPrecision(3) == parseFloat((x + 1).toPrecision(3))) {
                            $intervalList.removeClass('half-selected');
                            jQuery(this).addClass('half-selected');
                        }
                    });
                },
                callback: function(x, y) {
                    $intervalList.removeClass('selected');
                    var percent = (1 + x);
                    _update(percent);
                },
                speed: 50
            });

            $intervalList.click(function() {
                $intervalList.removeClass('selected');
                _update(jQuery(this).addClass('selected').data('ratio'), true);
            });

            jQuery(".temperaments").selectBox({
                menuTransition: 'slide',
                menuSpeed: 'fast'
            }).change(function() {
                _updateTemperament(jQuery(this).val());
            });

            jQuery('.replay').click(function() {
                _playNotes($hertz.val());
            });

            //start off at unison
            _update(1);
        }

    });
})(jQuery);

jQuery(document).ready(function() {
    monochord.init();

    jQuery('.logo').hover(function() {
        jQuery('footer').animate({
            height: 107
        });
    }, function() {
        jQuery('footer').animate({
            height: 20
        });
    });

});