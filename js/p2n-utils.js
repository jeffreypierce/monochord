var P2N = {};
P2N.utils = {
	/**
	 * Verifies nested object hierarchy
	 * @param {String} namespace eg "foo.bar"
	 * @param {Function|Object} assignment (optional). A value to assign to the
	 * provided namespace. If an object, it is merged with the existing
	 * namespace (if any) and returned. If a function, the function is called
	 * with the provided namespace as the first argument.
	 * @return {Object} namespace object
	 */

	namespace: function(namespace, assignment) {
		var parts = namespace.split("."),
			nsObj = window,
			i, l, p, err, hasOwn, isArray;

		err = "Namespace " + namespace + " contains a non-object value!";

		hasOwn = function(o, p) {
			if(typeof o === 'object' && typeof p === 'string') {
				return Object.prototype.hasOwnProperty.call(o, p);
			} else {
				return false;
			}
		};

		isArray = function(a) {
			return Object.prototype.toString.call(a) === '[object Array]';
		};

		for(i = 0, l = parts.length; i < l; i += 1) {
			p = parts[i];

			if(nsObj[p] === undefined) {
				nsObj[p] = {};
			} else if(typeof(nsObj[p]) != "object") {
				throw new Error(err);
			}
			nsObj = nsObj[p];
		}

		if(typeof assignment === 'function') {
			assignment(nsObj);
		} else if(typeof assignment === 'object' && !isArray(assignment)) {
			for(p in assignment) {
				if(hasOwn(assignment, p)) {
					nsObj[p] = assignment[p];
				}
			}
		}

		return nsObj;
	},

	roundToPlace: function(num, places) {
		var placesToTens = Math.pow(10, places);
		return(num * placesToTens) / placesToTens;
	},

	/**
	 * returns a ratio or fraction from a float
	 * @param {Number} number
	 * @param {String} delineator (optional)
	 * @return {String} ratio string
	 */
	getRatioFromNumber: function(number, delineator) {
		var delin = delineator || ':',
			ratio = '0',
			numerator, denominator, getFractionArray = function(num) {
			var hasWhole = false,
				interationLimit = 1000,
				accuracy = 0.001,
				fractionArray = [];

			if(num >= 1) {
				hasWhole = true;
				fractionArray.push(Math.floor(num));
			}
			if(num - Math.floor(num) === 0) {
				return fractionArray;
			}
			if(hasWhole) {
				num = num - Math.floor(num);
			}
			var decimal = num - parseInt(num, 10);
			var q = decimal;
			var i = 0;
			while(Math.abs(q - Math.round(q)) > accuracy) {
				if(i === interationLimit) {
					return false;
				}
				i++;
				q = i / decimal;
			}

			fractionArray.push(Math.round(q * num));
			fractionArray.push(Math.round(q));
			return fractionArray;
		};

		if(number || number != Infinity) {
			var fractionArray = getFractionArray(number);
			switch(fractionArray.length) {
			case 1:
				numerator = number;
				denominator = 1;
				break;
			case 2:
				numerator = fractionArray[0];
				denominator = fractionArray[1];
				break;
			case 3:
				numerator = fractionArray[0] * fractionArray[2] + fractionArray[1];
				denominator = fractionArray[2];
				break;
			}
			ratio = numerator + delin + denominator;
		}
		return ratio;
	}
};