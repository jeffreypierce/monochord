(function(){
    //initialize audio
    var context = new webkitAudioContext(),
        output = context.createGainNode();

    output.connect(context.destination);
    
    var Pluck = function(){
        this.pluck = context.createBufferSource();
        this.pluck.connect(output);
    };

    Pluck.prototype.play = function(freq, amp){
        //simple Karplus Strong Algorithm
         var ksBuffer =  (function(freq, amp) {
            var prevIndex = 0,
                buffer = [],
                arrayBuffer = new Float32Array(context.sampleRate),
                period = Math.floor(context.sampleRate / freq);

            var generateSample = function() {
                var index = buffer.shift();
                var sample = (index + prevIndex) / 2;
                prevIndex = sample;
                buffer.push(sample);
                return sample;
            };

            // generate noise
            for(var i = 0; i < period; i++) {
                var rand = Math.random();
                buffer[i] = ((rand > 0.5) ? 1 : -1) * rand;
            }

            for(var j = 0; j < context.sampleRate; j++) {
                arrayBuffer[j] = generateSample();
                var decay = amp - (j / context.sampleRate) * amp;
                arrayBuffer[j] = arrayBuffer[j] * decay;
            }
            return arrayBuffer;
        }(freq, amp));
        
        var audioBuffer = context.createBuffer(1, ksBuffer.length, context.sampleRate);
    
        audioBuffer.getChannelData(0).set(ksBuffer);
        this.pluck.buffer = audioBuffer;
        this.pluck.noteOn(0);
    };

    //public
    P2N.utils.namespace('monochord.pluck', {
        playNote: function(freq, amp){
            var pluck = new Pluck();
            pluck.play(freq, amp);
        }
    });
})();